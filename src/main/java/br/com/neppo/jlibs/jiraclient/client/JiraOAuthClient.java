package br.com.neppo.jlibs.jiraclient.client;

import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.auth.oauth.*;
import com.google.api.client.http.*;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Scanner;

public class JiraOAuthClient implements JiraClient {

    private String consumerKey;
    private String privateKey;
    private String baseUrl;
    private String callback;

    private static final String ACCESS_TOKEN_URL = "/plugins/servlet/oauth/access-token";
    private static final String AUTHORIZATION_URL = "/plugins/servlet/oauth/authorize";
    private static final String REQUEST_TOKEN_URL = "/plugins/servlet/oauth/request-token";

    public JiraOAuthClient(String consumerKey, String privateKey, String baseUrl, String callback) {
        this.consumerKey = consumerKey;
        this.privateKey = privateKey;
        this.baseUrl = baseUrl;
        this.callback = callback;
    }

    // ------------------------------ PUBLIC METHODS --------------------------

    public String getAuthorizationURL() throws JiraException {

        try {

            JiraOAuthGetTemporaryToken temporaryToken = new JiraOAuthGetTemporaryToken(baseUrl + REQUEST_TOKEN_URL);
            temporaryToken.consumerKey = consumerKey;
            temporaryToken.signer = getOAuthRsaSigner(privateKey);
            temporaryToken.transport = new ApacheHttpTransport();
            temporaryToken.callback = callback;
            OAuthCredentialsResponse response = temporaryToken.execute();

            OAuthAuthorizeTemporaryTokenUrl authorizationURL = new OAuthAuthorizeTemporaryTokenUrl(baseUrl + AUTHORIZATION_URL);
            authorizationURL.temporaryToken = response.token;
            return authorizationURL.toString();

        } catch (HttpResponseException e) {
            throw new JiraException(e.getMessage(), e);
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new JiraException(e);
        }
    }

    public String getAccessToken(String temporaryToken, String verifier) throws JiraException {

        try {

            JiraOAuthGetAccessToken oAuthGetAccessToken = new JiraOAuthGetAccessToken(baseUrl + ACCESS_TOKEN_URL);
            oAuthGetAccessToken.consumerKey = consumerKey;
            oAuthGetAccessToken.signer = getOAuthRsaSigner(privateKey);
            oAuthGetAccessToken.transport = new ApacheHttpTransport();
            oAuthGetAccessToken.verifier = verifier;
            oAuthGetAccessToken.temporaryToken = temporaryToken;

            OAuthCredentialsResponse response = oAuthGetAccessToken.execute();

            return response.token;

        } catch (HttpResponseException e) {
            throw new JiraException(e.getMessage(), e);
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new JiraException(e);
        }
    }

    public JsonNode executeRequest(String token, String endpoint, String method, Object body) throws JiraException {

        if (body != null && !(body instanceof JsonNode)) {
            throw new JiraException("Body must be a instance of JsonNode");
        }

        try {

            JiraOAuthGetAccessToken oAuthAccessToken = new JiraOAuthGetAccessToken(baseUrl +
                    ACCESS_TOKEN_URL);
            oAuthAccessToken.consumerKey = consumerKey;
            oAuthAccessToken.temporaryToken = token;
            oAuthAccessToken.signer = getOAuthRsaSigner(privateKey);
            oAuthAccessToken.transport = new ApacheHttpTransport();
            OAuthParameters parameters = oAuthAccessToken.createParameters();
            JsonNode response = getResponseFromUrl(parameters, new GenericUrl(baseUrl + endpoint), method,
                    body != null ? body.toString() : null);
            return response;

        } catch (HttpResponseException e) {
            throw new JiraException(e.getMessage(), e);
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new JiraException(e);
        }
    }

    // ------------------------------ PRIVATE METHODS -------------------------

    private OAuthRsaSigner getOAuthRsaSigner(String privateKey)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        OAuthRsaSigner oAuthRsaSigner = new OAuthRsaSigner();
        oAuthRsaSigner.privateKey = getPrivateKey(privateKey);
        return oAuthRsaSigner;
    }

    private PrivateKey getPrivateKey(String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] privateBytes = Base64.decodeBase64(privateKey);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpec);
    }

    private static JsonNode getResponseFromUrl(OAuthParameters parameters, GenericUrl url, String method, String data)
            throws IOException, JiraException {

        HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory(parameters);
        HttpRequest request;

        switch(method) {
            case "GET":
                request = requestFactory.buildGetRequest(url);
                break;
            case "POST":
                request = requestFactory.buildPostRequest(url,
                        ByteArrayContent.fromString("application/json", data));
                break;
            case "PUT":
                request = requestFactory.buildPutRequest(url,
                        ByteArrayContent.fromString("application/json", data));
                break;
            case "DELETE":
                request = requestFactory.buildDeleteRequest(url);
                break;
            default:
                throw new JiraException("Invalid HTTP method");
        }

        ObjectMapper mapper = new ObjectMapper();

        return mapper.readTree(parseResponse(request.execute()));
    }

    private static String parseResponse(HttpResponse response) throws IOException {
        Scanner s = new Scanner(response.getContent()).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";
        try {
            return new JSONObject(result).toString(2);
        } catch (JSONException e) {
            return result;
        }
    }

    // ------------------------------ GETTERS & SETTERS -----------------------

    public String getConsumerKey() {
        return consumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

}

