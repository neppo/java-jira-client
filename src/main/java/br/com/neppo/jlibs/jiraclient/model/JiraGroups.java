package br.com.neppo.jlibs.jiraclient.model;

import java.util.List;

public class JiraGroups {

    private String total;
    private List<JiraGroupsType> groups;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<JiraGroupsType> getGroups() {
        return groups;
    }

    public void setGroups(List<JiraGroupsType> groups) {
        this.groups = groups;
    }
}
