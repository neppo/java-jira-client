package br.com.neppo.jlibs.jiraclient.model;

import java.util.List;

public class JiraProject {

    private String id;
    private String key;
    private String name;
    private List<JiraIssuetype> issuetypes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<JiraIssuetype> getIssuetypes() {
        return issuetypes;
    }

    public void setIssuetypes(List<JiraIssuetype> issuetypes) {
        this.issuetypes = issuetypes;
    }
}
