package br.com.neppo.jlibs.jiraclient;

import br.com.neppo.jlibs.jiraclient.client.JiraBasicClient;
import br.com.neppo.jlibs.jiraclient.client.JiraClient;
import br.com.neppo.jlibs.jiraclient.client.JiraOAuthClient;
import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import br.com.neppo.jlibs.jiraclient.model.JiraWorklog;
import br.com.neppo.jlibs.jiraclient.util.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;

public class JiraWorklogService {

    private JiraClient jiraClient;

    public JiraWorklogService(String consumerKey, String privateKey, String baseUrl, String callback) {

        this.jiraClient = new JiraOAuthClient(consumerKey, privateKey, baseUrl, callback);

    }

    public JiraWorklogService(String baseUrl) {

        this.jiraClient = new JiraBasicClient(baseUrl);

    }

    public JiraWorklog create(String token, JiraWorklog jiraWorklog) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/issue/" + jiraWorklog.getIssueKey() + "/worklog",
                "POST",
                JSONUtil.jiraWorklogToJsonNode(jiraWorklog));

        return JSONUtil.jsonNodeToJiraWorklog(response);

    }
}
