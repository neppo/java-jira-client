package br.com.neppo.jlibs.jiraclient;

import br.com.neppo.jlibs.jiraclient.client.JiraBasicClient;
import br.com.neppo.jlibs.jiraclient.client.JiraClient;
import br.com.neppo.jlibs.jiraclient.client.JiraOAuthClient;
import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import br.com.neppo.jlibs.jiraclient.model.JiraUser;
import br.com.neppo.jlibs.jiraclient.util.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;

public class JiraUserService {

    private JiraClient jiraClient;

    public JiraUserService(String consumerKey, String privateKey, String baseUrl, String callback) {

        this.jiraClient = new JiraOAuthClient(consumerKey, privateKey, baseUrl, callback);

    }

    public JiraUserService(String baseUrl) {

        this.jiraClient = new JiraBasicClient(baseUrl);

    }

    public JiraUser getJiraUser(String token) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/myself",
                "GET",
                null);

        return JSONUtil.jsonNodeToJiraUser(response);

    }

}
