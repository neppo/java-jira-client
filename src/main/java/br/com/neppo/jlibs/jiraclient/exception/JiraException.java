package br.com.neppo.jlibs.jiraclient.exception;

public class JiraException extends Exception {

    public JiraException(String message) {
        super(message);
    }

    public JiraException(Throwable cause) {
        super(cause);
    }

    public JiraException(String message, Throwable cause) {
        super(message, cause);
    }
}