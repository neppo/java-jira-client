package br.com.neppo.jlibs.jiraclient;

import br.com.neppo.jlibs.jiraclient.client.JiraOAuthClient;
import br.com.neppo.jlibs.jiraclient.exception.JiraException;

public class JiraOAuthService {

    private JiraOAuthClient jiraOAuthClient;

    public JiraOAuthService(String consumerKey, String privateKey, String baseUrl, String callback) {

        this.jiraOAuthClient = new JiraOAuthClient(consumerKey, privateKey, baseUrl, callback);
    }

    public String getAuthorizationURL() throws JiraException {

        return jiraOAuthClient.getAuthorizationURL();

    }

    public String getAccessTokenByTemporaryToken(String temporaryToken, String verifier) throws JiraException {

        return jiraOAuthClient.getAccessToken(temporaryToken, verifier);

    }

}
