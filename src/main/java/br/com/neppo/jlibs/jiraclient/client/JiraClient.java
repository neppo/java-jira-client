package br.com.neppo.jlibs.jiraclient.client;

import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import com.fasterxml.jackson.databind.JsonNode;

public interface JiraClient {

    JsonNode executeRequest(String token, String endpoint, String method, Object body) throws JiraException;

}
