package br.com.neppo.jlibs.jiraclient.client;

import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import java.io.IOException;

public class JiraMultipartEntityClient implements JiraClient {


    private String baseUrl;

    public JiraMultipartEntityClient(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Override
    public JsonNode executeRequest(String token, String endpoint, String method, Object body) throws JiraException {

        if (body != null && !(body instanceof ContentBody)) {
            throw new JiraException("Body must be a instance of ContentBody");
        }

        CloseableHttpClient closeableHttpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(baseUrl + endpoint);
        httpPost.setHeader("X-Atlassian-Token", "nocheck");
        httpPost.setHeader("Authorization","Basic " + token);

        HttpEntity entity = MultipartEntityBuilder.create()
                .addPart("file", (ContentBody) body)
                .build();

        httpPost.setEntity(entity);

        CloseableHttpResponse response;

        try {
            response = closeableHttpClient.execute(httpPost);
        } catch (IOException e) {
            throw new JiraException(e.getMessage());
        }

        try {
            closeableHttpClient.close();
        } catch (IOException e) {
            throw new JiraException(e.getMessage());
        }

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new JiraException(response.getStatusLine().getReasonPhrase());
        }

        JSONObject jsonObject = new JSONObject(response);
        try {
            return new ObjectMapper().readTree(jsonObject.toString());
        } catch (IOException e) {
            throw new JiraException(e.getMessage());
        }


    }
}
