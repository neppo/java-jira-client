package br.com.neppo.jlibs.jiraclient.model;

import java.util.List;

public class JiraUser {

    private String username;
    private String email;
    private String displayName;
    private List<JiraGroupsType> groups;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<JiraGroupsType> getGroups() {
        return groups;
    }

    public void setGroups(List<JiraGroupsType> groups) {
        this.groups = groups;
    }
}
