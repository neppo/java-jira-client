package br.com.neppo.jlibs.jiraclient;

import br.com.neppo.jlibs.jiraclient.client.JiraBasicClient;
import br.com.neppo.jlibs.jiraclient.client.JiraClient;
import br.com.neppo.jlibs.jiraclient.client.JiraOAuthClient;
import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import br.com.neppo.jlibs.jiraclient.model.JiraIssue;
import br.com.neppo.jlibs.jiraclient.model.JiraIssuePaginated;
import br.com.neppo.jlibs.jiraclient.model.JiraProject;
import br.com.neppo.jlibs.jiraclient.model.JiraUser;
import br.com.neppo.jlibs.jiraclient.util.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.util.escape.CharEscapers;

import java.io.IOException;
import java.util.List;

public class JiraIssueService {

    private JiraClient jiraClient;

    public JiraIssueService(String consumerKey, String privateKey, String baseUrl, String callback) {

        this.jiraClient = new JiraOAuthClient(consumerKey, privateKey, baseUrl, callback);

    }

    public JiraIssueService(String baseUrl) {

        this.jiraClient = new JiraBasicClient(baseUrl);

    }

    public JiraIssue create(String token, JiraIssue jiraIssue) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/issue",
                "POST",
                JSONUtil.jiraIssueToJsonNode(jiraIssue));

        return JSONUtil.jsonNodeToJiraIssue(response);

    }

    public JiraIssue getByKey(String token, String key) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/issue/" + key,
                "GET",
                null);

        return JSONUtil.jsonNodeToJiraIssue(response);

    }

    public List<JiraProject> getMetadata(String token) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/issue/createmeta",
                "GET",
                null);

        return JSONUtil.jsonNodeToJiraProjectList(response);

    }

    public JiraProject getMetadataByProjectKey(String token, String projectKeys) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/issue/createmeta?projectKeys=" + projectKeys +
                        "&expand=projects.issuetypes.fields",
                "GET",
                null);

        return JSONUtil.jsonNodeToJiraProjectList(response.get("projects")).get(0);

    }

    public List<JiraIssue> findAll(String token) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/myself",
                "GET",
                null);

        JiraUser jiraUser = JSONUtil.jsonNodeToJiraUser(response);
        String query = CharEscapers.escapeUriQuery("assignee=" + jiraUser.getUsername());

        response = jiraClient.executeRequest(
                token,
                "/rest/api/2/search?jql=" + query ,
                "GET",
                null);


        JsonNode issuesList;
        try {
            issuesList = new ObjectMapper().readTree(response.toString()).get("issues");
        } catch (IOException e) {
            throw new JiraException(e);
        }

        return JSONUtil.jsonNodeToJiraIssueList(issuesList);
    }

    public List<JiraIssue> getIssuesByUser(String token, String query, String username) throws JiraException {

        String formatedQuery = CharEscapers.escapeUriQuery(query + username);

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/search?&maxResults=50&jql=" + formatedQuery + "&expand=transitions,changelog",
                "GET",
                null);

        return JSONUtil.jsonNodeToJiraIssueList(response.get("issues"));

    }

    public JsonNode getRawIssue(String token, String query, String username) throws JiraException {

        String formattedQuery = CharEscapers.escapeUriQuery(query + username);

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/search?&maxResults=50&jql=" + formattedQuery + "&expand=transitions,changelog",
                "GET",
                null);

        return response;
    }

}
