package br.com.neppo.jlibs.jiraclient.model;

public class JiraAttachment {

    private String self;
    private String fileName;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
