package br.com.neppo.jlibs.jiraclient.model;

public class TransitionsIssues {

    private String previous;
    private String later;

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getLater() {
        return later;
    }

    public void setLater(String later) {
        this.later = later;
    }
}
