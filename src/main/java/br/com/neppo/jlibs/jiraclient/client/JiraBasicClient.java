package br.com.neppo.jlibs.jiraclient.client;

import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import java.net.URI;
import java.util.List;

public class JiraBasicClient implements JiraClient {

    private String baseUrl;

    public JiraBasicClient(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    // ------------------------------ PUBLIC METHODS --------------------------

    public JsonNode executeRequest(String token, String endpoint, String method, Object body)
            throws JiraException {

        if (body != null && !(body instanceof JsonNode)) {
            throw new JiraException("Body must be a instance of JsonNode");
        }

        JsonNode response;

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Basic " + token);
        headers.set("Content-Type", "application/json");

        switch (method) {
            case "GET":
                response = get(URI.create(baseUrl + endpoint), headers);
                break;
            case "POST":
                response = post(URI.create(baseUrl + endpoint), (JsonNode) body, headers);
                break;
            case "DELETE":
                response = delete(URI.create(baseUrl + endpoint), headers);
                break;
            default:
                throw new JiraException("Invalid HTTP method");

        }


        return response;
    }

    // ------------------------------ PRIVATE METHODS -------------------------


    private static JsonNode get(URI url, HttpHeaders headers) throws JiraException {
        ResponseEntity<JsonNode> responseEntity;

        try {
            responseEntity = new RestTemplate().exchange(url, HttpMethod.GET, new HttpEntity<JsonNode>(headers),
                    JsonNode.class);

            List<String> causes = responseEntity.getHeaders().get("X-Seraph-LoginReason");
            for (String cause : causes) {
                if (cause.equals("AUTHENTICATION_DENIED")) {
                    throw new JiraException("Authentication denied due to CAPTCHA");
                }
            }

        } catch (HttpStatusCodeException e) {
            throw new JiraException(e.getMessage(), e);
        }

        return responseEntity.getBody();
    }

    private JsonNode post(URI url, JsonNode requestBody, HttpHeaders headers) throws JiraException {


        JsonNode response;

        HttpEntity<JsonNode> request = new HttpEntity<>(requestBody, headers);

        try {
            response = new RestTemplate().postForObject(url, request, JsonNode.class);
        } catch (HttpStatusCodeException e) {
            throw new JiraException(e.getMessage(), e);
        }

        return response;
    }

    private static JsonNode delete(URI url, HttpHeaders headers) throws JiraException {
        ResponseEntity<JsonNode> responseEntity;

        try {
            responseEntity = new RestTemplate().exchange(url, HttpMethod.DELETE, new HttpEntity<JsonNode>(headers),
                    JsonNode.class);

            List<String> causes = responseEntity.getHeaders().get("X-Seraph-LoginReason");
            for (String cause : causes) {
                if (cause.equals("AUTHENTICATION_DENIED")) {
                    throw new JiraException("Authentication denied due to CAPTCHA");
                }
            }

        } catch (HttpStatusCodeException e) {
            throw new JiraException(e.getMessage(), e);
        }

        return responseEntity.getBody();
    }

    // ------------------------------ GETTERS & SETTERS -----------------------

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
