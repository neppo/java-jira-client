package br.com.neppo.jlibs.jiraclient.model;

import java.util.List;

public class JiraIssuetype {

    private String name;
    private String id;
    private List<JiraIssueField> jiraIssueFields;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<JiraIssueField> getJiraIssueFields() {
        return jiraIssueFields;
    }

    public void setJiraIssueFields(List<JiraIssueField> jiraIssueFields) {
        this.jiraIssueFields = jiraIssueFields;
    }
}
