package br.com.neppo.jlibs.jiraclient.client;

import com.google.api.client.auth.oauth.OAuthGetAccessToken;

class JiraOAuthGetAccessToken extends OAuthGetAccessToken {

    JiraOAuthGetAccessToken(String authorizationServerUrl) {
        super(authorizationServerUrl);
        this.usePost = true;
    }

}
