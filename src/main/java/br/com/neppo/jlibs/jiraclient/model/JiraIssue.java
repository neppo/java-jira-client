package br.com.neppo.jlibs.jiraclient.model;

import java.util.List;

public class JiraIssue {

    private String id;
    private String key;
    private String self;
    private String summary;
    private String projectId;
    private String issuetypeId;
    private String description;
    private String status;
    private String creationDate;
    private List<JiraHistories> jiraHistories;

    public JiraIssue() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getIssuetypeId() {
        return issuetypeId;
    }

    public void setIssuetypeId(String issuetypeId) {
        this.issuetypeId = issuetypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public List<JiraHistories> getJiraHistories() {
        return jiraHistories;
    }

    public void setJiraHistories(List<JiraHistories> jiraHistories) {
        this.jiraHistories = jiraHistories;
    }
}
