package br.com.neppo.jlibs.jiraclient;

import br.com.neppo.jlibs.jiraclient.client.JiraBasicClient;
import br.com.neppo.jlibs.jiraclient.client.JiraClient;
import br.com.neppo.jlibs.jiraclient.client.JiraOAuthClient;
import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import br.com.neppo.jlibs.jiraclient.model.JiraGroups;
import br.com.neppo.jlibs.jiraclient.model.JiraUser;
import br.com.neppo.jlibs.jiraclient.util.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class JiraGroupsService {

    private JiraClient jiraClient;

    public JiraGroupsService(String consumerKey, String privateKey, String baseUrl, String callback) {

        this.jiraClient = new JiraOAuthClient(consumerKey, privateKey, baseUrl, callback);

    }

    public JiraGroupsService(String baseUrl) {

        this.jiraClient = new JiraBasicClient(baseUrl);

    }

    public JiraGroups getJiraGroups(String token) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/groups/picker?maxResults=100",
                "GET",
                null);

        return JSONUtil.jsonNodeToJiraGroups(response);

    }

    public JiraUser getGroupsByUser(String token, String username) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/user?username=" + username + "&expand=groups",
                "GET",
                null
        );
        return JSONUtil.jsonNodeToJiraUser(response);
    }

    public JiraGroups saveUserInJiraGroup(String token, String group ,String username) throws JiraException{
        String groupname = null;
        try {
            groupname = URLEncoder.encode(group, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.getMessage();
        }

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/group/user?groupname=" + groupname,
                "POST",
                JSONUtil.jiraUserToJsonNode(username));
        return JSONUtil.jsonNodeToJiraGroups(response);
    }

    public void deleteUserInJiraGroup(String token, String group, String username) throws JiraException{
        String groupname = null;
        try {
            groupname = URLEncoder.encode(group, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.getMessage();
        }

        jiraClient.executeRequest(
                token,
                "/rest/api/2/group/user?username=" + username + "&groupname=" + groupname,
                "DELETE",
                null
        );
    }
}
