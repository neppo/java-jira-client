package br.com.neppo.jlibs.jiraclient.util;

import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import br.com.neppo.jlibs.jiraclient.model.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class JSONUtil {

    public static JiraIssue jsonNodeToJiraIssue(JsonNode jsonNode) {

        String id = jsonNode.has("id") ? jsonNode.get("id").asText() : null;
        String key = jsonNode.has("key") ? jsonNode.get("key").asText() : null;
        String self = jsonNode.has("self") ? jsonNode.get("self").asText() : null;
        JsonNode fields = jsonNode.has("fields") ? jsonNode.get("fields") : null;
        String summary = fields != null && fields.has("summary") ? fields.get("summary").asText() : null;
        JsonNode statusFields = fields.has("status") ? fields.get("status") : null;
        String status = statusFields != null && statusFields.has("name") ? statusFields.get("name").asText() : null;
        JsonNode issueType = fields.has("issuetype") ? fields.get("issuetype") : null;
        String description = issueType != null && issueType.has("description") ? issueType.get("description").asText() : null;
        String creationDate = fields != null && fields.has("created")  ? fields.get("created").asText() : null;
        String date = formatDate(creationDate);
        JsonNode issueChangeLog = jsonNode.has("changelog") ? jsonNode.get("changelog") : null;
        JsonNode issueHistories = issueChangeLog.has("histories") ? issueChangeLog.get("histories") : null;

        JiraIssue jiraIssue = new JiraIssue();
        jiraIssue.setId(id);
        jiraIssue.setKey(key);
        jiraIssue.setSelf(self);
        jiraIssue.setSummary(summary);
        jiraIssue.setStatus(status);
        jiraIssue.setDescription(description);
        jiraIssue.setCreationDate(date);
        jiraIssue.setJiraHistories(jsonNodeToJiraHistoriesList(issueHistories));
        return jiraIssue;
    }

    private static List<JiraHistories> jsonNodeToJiraHistoriesList(JsonNode jsonNode){
        List<JiraHistories> jiraHistories = new ArrayList<>();
        jsonNode.forEach((JsonNode issueJsonHistories) -> {
            jiraHistories.add(jsonNodeToJiraHistories(issueJsonHistories));
        });
        return jiraHistories;
    }

    private static JiraHistories jsonNodeToJiraHistories(JsonNode jsonNodeHistories){
        String id = jsonNodeHistories.has("id") ? jsonNodeHistories.get("id").asText() : null;
        JsonNode authorObject = jsonNodeHistories.has("author") ? jsonNodeHistories.get("author"): null;
        String authorName = authorObject.has("name") ? authorObject.get("name").asText() : null;
        String createdDate = jsonNodeHistories.has("created") ? jsonNodeHistories.get("created").asText() : null;
        JsonNode itemsTransitions = jsonNodeHistories.has("items") ? jsonNodeHistories.get("items") :null;

        JiraHistories jiraHistories = new JiraHistories();
        jiraHistories.setId(id);
        jiraHistories.setAuthor(authorName);
        jiraHistories.setCreatedDate(formatDate(createdDate));
        jiraHistories.setTransitionsIssues(jsonNodeToHistoriesList(itemsTransitions));
        return jiraHistories;
    }

    private static TransitionsIssues jsonNodeToJiraHistoriesTransitions(JsonNode jsonNodeHistories){
        String previous = jsonNodeHistories.has("fromString") ? jsonNodeHistories.get("fromString").asText() : null;
        String later = jsonNodeHistories.has("toString") ? jsonNodeHistories.get("toString").asText() : null;
        TransitionsIssues transitionsIssues = new TransitionsIssues();
        transitionsIssues.setPrevious(previous);
        transitionsIssues.setLater(later);
        return transitionsIssues;
    }

    private static List<TransitionsIssues> jsonNodeToHistoriesList(JsonNode jsonNodeHistories){
        List<TransitionsIssues> jiraHistories = new ArrayList<>();
        jsonNodeHistories.forEach((JsonNode issueJsonHistories) -> {
            jiraHistories.add(jsonNodeToJiraHistoriesTransitions(issueJsonHistories));
        });
        return jiraHistories;
    }


    public static String formatDate(String creationDate) {

        String date = creationDate.split("T")[0];
        String hour = creationDate.substring(11,19);
        String dateHour = date + " " + hour;
        String dateFormated;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date newDate = null;
        try {
            newDate = dateFormat.parse(dateHour);
        } catch (ParseException e) {
            e.getMessage();
        }
        dateFormat.applyPattern("dd-MM-yyyy HH:mm:ss");
        dateFormated = dateFormat.format(newDate);

        return dateFormated;
    }

    public static List<JiraIssue> jsonNodeToJiraIssueList(JsonNode jsonNode) {

        List<JiraIssue> jiraIssues = new ArrayList<>();
        jsonNode.forEach((JsonNode issueJson) -> {
            jiraIssues.add(jsonNodeToJiraIssue(issueJson));
        });
        return jiraIssues;

    }

    public static List<JiraProject> jsonNodeToJiraProjectList(JsonNode jsonNode) {

        List<JiraProject> jiraProjects = new ArrayList<>();
        if (jsonNode != null && jsonNode.isArray()) {
            jsonNode.forEach((JsonNode jiraProjectJson) -> {
                JiraProject jiraProject = jsonNodeToJiraProject(jiraProjectJson);
                jiraProjects.add(jiraProject);
            });
        }
        return jiraProjects;
    }

    public static JiraIssuetype jsonNodeToJiraIssuetype(JsonNode jiraIssueJson) {

        String issueId = jiraIssueJson.has("id") ? jiraIssueJson.get("id").asText() : null;
        String issueName = jiraIssueJson.has("name") ? jiraIssueJson.get("name").asText() : null;
        JiraIssuetype jiraIssuetype = new JiraIssuetype();
        jiraIssuetype.setId(issueId);
        jiraIssuetype.setName(issueName);

        JsonNode fieldsList = jiraIssueJson.has("fields") ? jiraIssueJson.get("fields") : null;
        jiraIssuetype.setJiraIssueFields(jsonNodeToJiraIssueFieldList(fieldsList));

        return jiraIssuetype;

    }

    public static List<JiraIssueField> jsonNodeToJiraIssueFieldList(JsonNode jsonNode) {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> treeMap = null;
        try {
            treeMap = mapper.readValue(jsonNode.toString(), Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> keys  = new ArrayList<>();

        if (treeMap != null) {
            treeMap.forEach((key, value) -> {
                keys.add(key);
            });
        }

        List<JiraIssueField> jiraIssueFields = new ArrayList<>();

        keys.forEach((String key) -> {
            JiraIssueField jiraIssueField = jsonNodeToJiraIssueField(jsonNode.get(key));
            jiraIssueField.setKey(key);
            jiraIssueFields.add(jiraIssueField);
        });

        return jiraIssueFields;

    }

    private static List<String> findKeys(Map<String, Object> treeMap , List<String> keys) {
        treeMap.forEach((key, value) -> {
            keys.add(key);
        });
        return keys;
    }


    public static JiraIssueField jsonNodeToJiraIssueField(JsonNode jiraIssueFieldJson) {
        String name = jiraIssueFieldJson.has("name") ? jiraIssueFieldJson.get("name").asText() : null;
        Boolean required = jiraIssueFieldJson.has("required") ? jiraIssueFieldJson.get("required").asBoolean(false) : null;
        JiraIssueField jiraIssueField = new JiraIssueField();
        jiraIssueField.setName(name);
        jiraIssueField.setRequired(required);
        return jiraIssueField;
    }

    public static JiraProject jsonNodeToJiraProject(JsonNode jsonNode) {


        String key = jsonNode.has("key") ? jsonNode.get("key").asText() : null;
        String id = jsonNode.has("id") ? jsonNode.get("id").asText() : null;
        String name = jsonNode.has("name") ? jsonNode.get("name").asText() : null;
        JiraProject jiraProject = new JiraProject();
        jiraProject.setId(id);
        jiraProject.setKey(key);
        jiraProject.setName(name);

        JsonNode issueTypesList = jsonNode.has("issuetypes") ? jsonNode.get("issuetypes") : null;
        jiraProject.setIssuetypes(jsonNodeToJiraIssuetypeList(issueTypesList));

        return jiraProject;

    }

    public static List<JiraIssuetype> jsonNodeToJiraIssuetypeList(JsonNode jsonNode) {

        List<JiraIssuetype> jiraIssuetypes = new ArrayList<>();

        if (jsonNode != null && jsonNode.isArray()) {
            jsonNode.forEach((JsonNode jiraIssuetypeJson) -> {
                jiraIssuetypes.add(jsonNodeToJiraIssuetype(jiraIssuetypeJson));
            });
        }

        return jiraIssuetypes;
    }

    public static JsonNode jiraIssueToJsonNode(JiraIssue jiraIssue) {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode project = mapper.createObjectNode()
                .put("id", jiraIssue.getProjectId());
        ObjectNode issuetype = mapper.createObjectNode()
                .put("id", jiraIssue.getIssuetypeId());
        ObjectNode fields = mapper.createObjectNode()
                .putPOJO("issuetype", issuetype)
                .putPOJO("project", project)
                .put("summary", jiraIssue.getSummary())
                .put("description", jiraIssue.getDescription());
        return mapper.createObjectNode()
                .putPOJO("fields", fields);

    }

    public static JsonNode jiraWorklogToJsonNode(JiraWorklog jiraWorklog) {

        ObjectMapper mapper = new ObjectMapper();

        return mapper.createObjectNode()
                .put("timeSpent", jiraWorklog.getTimeSpent())
                .put("started", jiraWorklog.getStarted())
                .put("comment", jiraWorklog.getComment());

    }

    public static JsonNode jiraUserToJsonNode(String name) {

        ObjectMapper mapper = new ObjectMapper();

        return mapper.createObjectNode()
                .put("name", name);

    }

    public static JiraWorklog jsonNodeToJiraWorklog(JsonNode jsonNode) {

        String self = jsonNode.has("self") ? jsonNode.get("self").asText() : null;
        String id = jsonNode.has("id") ? jsonNode.get("id").asText() : null;
        JiraWorklog jiraWorklog = new JiraWorklog();
        jiraWorklog.setSelf(self);
        jiraWorklog.setId(id);
        return jiraWorklog;

    }

    public static JiraUser jsonNodeToJiraUser(JsonNode jsonNode) throws JiraException {

        String username = jsonNode.has("name") ? jsonNode.get("name").asText() : null;
        String displayName = jsonNode.has("displayName") ? jsonNode.get("displayName").asText() : null;
        String email = jsonNode.has("emailAddress") ? jsonNode.get("emailAddress").asText() : null;

        JiraUser jiraUser = new JiraUser();
        jiraUser.setUsername(username);
        jiraUser.setDisplayName(displayName);
        jiraUser.setEmail(email);
        JsonNode groupsList;
        try {
            groupsList = new ObjectMapper().readTree(jsonNode.toString()).get("groups").get("items");
        } catch (IOException e) {
            throw new JiraException(e);
        }
        jiraUser.setGroups(jsonNodeToJiraGroupList(groupsList));

        return jiraUser;

    }

    public static JiraGroups jsonNodeToJiraGroups(JsonNode jsonNode) {

        String total = jsonNode.has("total") ? jsonNode.get("total").asText() : null;
        JiraGroups jiraGroups = new JiraGroups();
        jiraGroups.setTotal(total);

        JsonNode groupsList = jsonNode.has("groups") ? jsonNode.get("groups") : null;
        jiraGroups.setGroups(jsonNodeToJiraGroupList(groupsList));
        return jiraGroups;

    }

    public static List<JiraGroupsType> jsonNodeToJiraGroupList(JsonNode jsonNode) {

        List<JiraGroupsType> jiraGroups = new ArrayList<>();
        if (jsonNode != null && jsonNode.isArray()) {
            jsonNode.forEach((JsonNode jiraGroupsJson) -> {
                jiraGroups.add(jsonNodeToGroups(jiraGroupsJson));
            });
        }
        return jiraGroups;
    }

    public static JiraGroupsType jsonNodeToGroups(JsonNode jiraGroupsJson) {

        String name = jiraGroupsJson.has("name") ? jiraGroupsJson.get("name").asText() : null;
        JiraGroupsType jiraGroupsType = new JiraGroupsType();
        jiraGroupsType.setName(name);
        return jiraGroupsType;
    }

    public static JiraAttachment jsonNodeToJiraAttachment(JsonNode jsonNode) {

        String self = jsonNode.has("self") ? jsonNode.get("self").asText() : null;
        JiraAttachment jiraAttachment = new JiraAttachment();
        jiraAttachment.setSelf(self);
        return jiraAttachment;

    }

}