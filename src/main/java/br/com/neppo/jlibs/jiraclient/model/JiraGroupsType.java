package br.com.neppo.jlibs.jiraclient.model;

public class JiraGroupsType {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
