package br.com.neppo.jlibs.jiraclient;

import br.com.neppo.jlibs.jiraclient.client.JiraBasicClient;
import br.com.neppo.jlibs.jiraclient.client.JiraClient;
import br.com.neppo.jlibs.jiraclient.client.JiraOAuthClient;
import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import br.com.neppo.jlibs.jiraclient.model.JiraProject;
import br.com.neppo.jlibs.jiraclient.util.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class JiraProjectService {

    private JiraClient jiraClient;

    public JiraProjectService(String consumerKey, String privateKey, String baseUrl, String callback) {

        this.jiraClient = new JiraOAuthClient(consumerKey, privateKey, baseUrl, callback);

    }

    public JiraProjectService(String baseUrl) {

        this.jiraClient = new JiraBasicClient(baseUrl);

    }

    public List<JiraProject> getRecentProjects(String token) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/project?recent=5",
                "GET",
                null);

        return JSONUtil.jsonNodeToJiraProjectList(response);

    }

    public JiraProject getByKey(String token, String key) throws JiraException {

        JsonNode response = jiraClient.executeRequest(
                token,
                "/rest/api/2/project/" + key,
                "GET",
                null);

        return JSONUtil.jsonNodeToJiraProject(response);

    }

}
