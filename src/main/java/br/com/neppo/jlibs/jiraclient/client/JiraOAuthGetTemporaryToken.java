package br.com.neppo.jlibs.jiraclient.client;

import com.google.api.client.auth.oauth.OAuthGetTemporaryToken;

class JiraOAuthGetTemporaryToken extends OAuthGetTemporaryToken {

    JiraOAuthGetTemporaryToken(String authorizationServerUrl) {
        super(authorizationServerUrl);
        this.usePost = true;
    }
}
