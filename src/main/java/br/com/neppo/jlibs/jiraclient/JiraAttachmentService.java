package br.com.neppo.jlibs.jiraclient;

import br.com.neppo.jlibs.jiraclient.client.JiraClient;
import br.com.neppo.jlibs.jiraclient.client.JiraMultipartEntityClient;
import br.com.neppo.jlibs.jiraclient.exception.JiraException;
import br.com.neppo.jlibs.jiraclient.model.JiraAttachment;
import br.com.neppo.jlibs.jiraclient.util.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.entity.mime.content.FileBody;
import java.io.File;

public class JiraAttachmentService {

    private JiraClient jiraClient;

    public JiraAttachmentService(String baseUrl) {

        this.jiraClient = new JiraMultipartEntityClient(baseUrl);

    }

    public JiraAttachment addAttachmentToIssue(String token, String issueKey, String fullFilename) throws JiraException {

        File fileToUpload = new File(fullFilename);
        FileBody fileBody = new FileBody(fileToUpload);

        JsonNode response = jiraClient.executeRequest(token,
                "/rest/api/2/issue/" + issueKey + "/attachments",
                null,
                fileBody);

        return JSONUtil.jsonNodeToJiraAttachment(response);

    }

}
