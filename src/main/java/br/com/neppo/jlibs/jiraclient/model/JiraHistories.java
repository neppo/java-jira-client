package br.com.neppo.jlibs.jiraclient.model;

import java.util.List;

public class JiraHistories {

    private String id;
    private String author;
    private String createdDate;
    private List<TransitionsIssues> transitionsIssues;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<TransitionsIssues> getTransitionsIssues() {
        return transitionsIssues;
    }

    public void setTransitionsIssues(List<TransitionsIssues> transitionsIssues) {
        this.transitionsIssues = transitionsIssues;
    }
}
