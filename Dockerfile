FROM openjdk:8-jdk

MAINTAINER Neppo "infra@neppo.com.br"

RUN apt-get update
RUN apt-get install locales -y

ENV ENCODING UTF-8
ENV LANG pt_BR.$ENCODING
ENV LC_ALL pt_BR.$ENCODING

RUN echo $LC_ALL $ENCODING >> /etc/locale.gen

ENV locale-gen $LC_ALL

RUN dpkg-reconfigure -f noninteractive locales

RUN apt-get install ruby ruby-dev g++ make bzip2 locales vim -y
RUN gem install sass compass

RUN echo "alias ll='ls -alF'" >> /etc/bash.bashrc

ENV USER_ID 1000
ENV MAVEN_VERSION 3.3.9
ENV CODE_PATH /c
ENV M2_REPO /m
ENV MAVEN_OPTS -Xmx2560m -Dmaven.artifact.threads=10
ENV MAVEN_FILE apache-maven-$MAVEN_VERSION

WORKDIR /opt

RUN wget http://ftp.unicamp.br/pub/apache/maven/maven-3/$MAVEN_VERSION/binaries/$MAVEN_FILE-bin.tar.gz
RUN tar -xvf $MAVEN_FILE-bin.tar.gz
RUN rm $MAVEN_FILE-bin.tar.gz

RUN mkdir $CODE_PATH
RUN mkdir $M2_REPO

RUN useradd -l -u $USER_ID -ms /bin/bash builder
RUN chown -R builder:builder $CODE_PATH

USER builder

WORKDIR $CODE_PATH

RUN ln -s $M2_REPO ~/.m2

ENV PATH $PATH:/opt/$MAVEN_FILE/bin